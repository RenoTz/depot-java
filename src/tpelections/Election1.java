package tpelections;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.Scanner;
import java.util.logging.Level;
import java.util.logging.Logger;

public class Election1 {

    public Election1() {
        try {
            String nomFichier = "resultats.txt";
            Scanner f = new Scanner(new File(nomFichier));
            // Lecture du nom des candidats
            String nom = f.nextLine();
            String[] candidats = nom.split(":");
            String villePrec = " ";
            int voixVille1 = 0, voixVille2 = 0, voixVille3 = 0;
            int voixTotal1 = 0, voixTotal2 = 0, voixTotal3 = 0;

            String grad = "*****";

            boolean premier = true;
            String tirets1 = "          +---------+---------+---------+\n";
            String tirets2 = "+---------+---------+---------+---------+\n";
            //Lecture de la ville - des bureaux - des voix(v)
            while (f.hasNextLine()) {
                // lecture d'une ligne et découpage en champs
                String ligne = f.nextLine();
                String[] champs = ligne.split(":");
                String ville = champs[0];
                String bureau = champs[1];
                int voixBureau1 = Integer.parseInt(champs[2]);
                int voixBureau2 = Integer.parseInt(champs[3]);
                int voixBureau3 = Integer.parseInt(champs[4]);

                //si on change de ville, afficher le nom de la ville
                // et l'entete du tableau des votes
                if (!ville.equals(villePrec)) {
                    if (!premier) {

                        System.out.format(tirets2);
                        System.out.println();
                        System.out.format("  Total : %8d %8d  %8d\n",
                                voixVille1, voixVille2, voixVille3);

                        voixVille1 = 0;
                        voixVille2 = 0;
                        voixVille3 = 0;
                    }

                    System.out.println();
                    System.out.format("%10s\n", ville);
                    System.out.format(tirets1);
                    System.out.format("          |%8s |%8s |%8s | \n",
                            candidats[0], candidats[1], candidats[2]);
                    System.out.format(tirets2);

                    villePrec = ville;
                }

                voixVille1 = voixBureau1 + voixVille1;
                voixVille2 = voixBureau2 + voixVille2;
                voixVille3 = voixBureau3 + voixVille3;
                voixTotal1 += voixBureau1;
                voixTotal2 += voixBureau2;
                voixTotal3 += voixBureau3;
                premier = false;

                // Lecture du bureau, des resultats                       
                System.out.format("|%8s |  %-6s |  %-6s |  %-6s |\n",
                        bureau, voixBureau1, voixBureau2, voixBureau3);
            }

            // calcul des pourcentagez obtenus
            float total = voixTotal1 + voixTotal2 + voixTotal3;
            float res1 = (voixTotal1 / total) * 100;
            float res2 = (voixTotal2 / total) * 100;
            float res3 = (voixTotal3 / total) * 100;

            System.out.format(tirets2);
            System.out.println();
            System.out.format("  Total : %8d %8d  %8d\n\n", voixVille1, voixVille2, voixVille3);
            System.out.format("%8s : %5d----> %%%2.2f\n", candidats[0], voixTotal1, res1);
            System.out.format("%8s : %5d----> %%%2.2f\n", candidats[1], voixTotal2, res2);
            System.out.format("%8s : %5d----> %%%2.2f\n", candidats[2], voixTotal3, res3);

            if (res1 > res2 && res1 > res3) {
                int resMax = Math.round(res1);
                if (res2 > res1 && res2 > res3) {
                    Math.round(res2);
                    resMax = Math.round(res2);
                } else {
                    Math.round(res3);
                    resMax = Math.round(res3);


                    //for (int i = 0; i <= resMax; i += 10) {
                        if (resMax < 100 && resMax >= 90) {
                            System.out.println();
                            if (resMax < 90 && resMax >= 80) {
                                System.out.println();
                                if (resMax < 80 && resMax >= 70) {
                                    System.out.println();
                                    if (resMax < 70 && resMax >= 60) {
                                        System.out.println();
                                        if (resMax < 60 && resMax >= 50) {
                                            System.out.println();
                                        }
                                    }
                                }
                            } else {
                                System.out.println("*****");
                            }
                      // }
                    }
                }
            }
        } catch (FileNotFoundException ex) {
            Logger.getLogger(Election1.class.getName()).
                    log(Level.SEVERE, null, ex);
        }
    }
}
